function userController($scope) {
$scope.fName = '';
$scope.lName = '';
$scope.instrument = '';
$scope.passw1 = '';
$scope.passw2 = '';
$scope.users = [
  {id:1, fName:'Jerry', lName:'Garcia'     , instrument:'Lead guitar, vocals'},
  {id:2, fName:'Bob',   lName:'Weir'       , instrument:'Rhythm guitar, vocals'},
  {id:3, fName:'Brent', lName:'Mydland'    , instrument:'Keyboards, vocals'},
  {id:4, fName:'Phil',  lName:'Lesh'       , instrument:'Bass, vocals'},
  {id:5, fName:'Bill',  lName:'Kreutzmann' , instrument:'Drums'},
  {id:6, fName:'Mickey',lName:'Hart'       , instrument:'Drums'}
];
$scope.edit = true;
$scope.error = false;
$scope.incomplete = false; 

$scope.editUser = function(id) {
  if (id == 'new') {
    $scope.edit = true;
    $scope.incomplete = true;
    $scope.fName = '';
    $scope.lName = '';
    $scope.instrument = '';
    } else {
    $scope.edit = false;
    $scope.fName = $scope.users[id-1].fName;
    $scope.lName = $scope.users[id-1].lName; 
    $scope.instrument = $scope.users[id-1].instrument; 
  }
};

$scope.$watch('passw1',     function() {$scope.test();});
$scope.$watch('passw2',     function() {$scope.test();});
$scope.$watch('fName',      function() {$scope.test();});
$scope.$watch('lName',      function() {$scope.test();});
$scope.$watch('instrument', function() {$scope.test();});

$scope.test = function() {
  if ($scope.passw1 !== $scope.passw2) {
    $scope.error = true;
    } else {
    $scope.error = false;
  }
  $scope.incomplete = false;
  if ($scope.edit && (!$scope.fName.length ||
  !$scope.lName.length || !$scope.instrument.length ||
  !$scope.passw1.length || !$scope.passw2.length)) {
       $scope.incomplete = true;
  }
};

}