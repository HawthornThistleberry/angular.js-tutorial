(function() {

  var app = angular.module('gemStore',['store-products']);

  app.controller('StoreController', [ '$http',function($http){
    // will be executed when the controller is called
    //this.products = gems;
    var store = this; // store the object for inside the other scope
    this.products = []; // start with no products so the page won't show errors while the JSON is loading
    $http.get('./products.json').success(function(data){
      store.products = data;
      // the binding will make this redraw the page automatically
    });
  }]);
  
  var gems = [
  {
    name: 'Dodecahedron',
    price: 2,
    description: 'Twelve sides, twenty vertices, hint of quintessence.',
    canPurchase: true,
    soldOut: false,
    images: [
      {
        full: 'dodecahedron-01-full.jpg',
        thumb: 'dodecahedron-01-thumb.jpg'
      },
      {
        full: 'dodecahedron-02-full.jpg',
        thumb: 'dodecahedron-02-thumb.jpg'
      }
    ],
    reviews: [
      {
        stars: 5,
        body: "I love this product!",
        author: "joe@thomas.com"
      },
      {
        stars: 1,
        body: "This product sucks.",
        author: "tim@hater.com"
      }
    ]
  },
  {
    name: 'Icosahedron',
    price: 5.95,
    description: 'Twenty sides, twelve vertices, rolls nicely.',
    canPurchase: false,
    soldOut: false
  }
  ];

  app.controller('ReviewController', function(){
    this.review = {};
    
    this.addReview = function(product) {
      product.reviews.push(this.review);
      this.review = {};
    };
  });  
    
    
})();
