function orderController($scope) {

  $scope.order = [
    {sku:1,  desc:'Cheese Whiz', qty:1, cost:11},
    {sku:12, desc:'Spam',        qty:1, cost:22},
    {sku:6,  desc:'Velveeta',    qty:2, cost:7 },
    {sku:33, desc:'Cool Whip',   qty:1, cost:3 },
    {sku:2,  desc:'Twinkies',    qty:4, cost:19}
	],
  
  $scope.invoice = function() {
    var i, t = 0;
    for (i = 0; i < $scope.order.length; i++) {
      t += $scope.order[i].qty * $scope.order[i].cost;
    }
    return t;
	}
  
  $scope.totalqty = function() {
    var i, t = 0;
    for (i = 0; i < $scope.order.length; i++) {
      t += $scope.order[i].qty;
    }
    return t;
	}

}
